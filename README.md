## Practice purpose
- SpringBoot integration with webflux, security, mongoDB.
- Front-end resource usage with thymeleaf.
- Docker build with gradle.

## Run container
```
docker run -p 8081:8081 -e SPRING_DATASOURCE_HOST='172.17.0.1' -e SPRING_DATASOURCE_PORT='27017' -e SPRING_DATASOURCE_NAME='test' -e SPRING_DATASOURCE_USERNAME='admin' -e SPRING_DATASOURCE_PASSWORD='admin1234' -e SERVER_PORT='8081' -e --name pighouse7647/click-board:latest clickboard
```
Notice the values of 'SPRING_DATASOURCE_HOST' and 'SPRING_DATASOURCE_PORT' are depend on the environment where your mongoDB run.
If your mongoDB is running on docker, then you can inspect the network config to find out the corresponding setting. 


## Register
- Go to http://localhost:8081/registry
- Rigister as a member by entering account and password.

## Read page
- Display a sentence randomly.
- Every word can be pressed.
- Every word has two numbers beside, both represent the click times as record.
- The difference between two numbers is that, the left one should be monotone increasing, the right one can be reset to zero.

## Status page
- Display all words' record.
- Once pressing certain word, its' current record will be reset.

## List page
- Show all users' record.

## Reference
[Image on Dockerhub](https://cloud.docker.com/u/pighouse7647/repository/docker/pighouse7647/click-board)