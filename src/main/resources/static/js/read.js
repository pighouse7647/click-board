function getSentencesArray() {
  let sentences = [];
  sentences.push('She commutes to the city by car every day');
  sentences.push('I cringed when I realized what I\'d said');
  return sentences;
}

function getWordElements(sentence, userId, gender, userName) {
  return sentence.split(' ').map(function (w) {

    let wordElement = elementCreator('button', w, 'mx-2 btn btn-primary'),
        wCountEle = elementCreator('span', '0', 'mx-2 mx-2badge badge-light'),
        wCurrentCountEle = elementCreator('span', '0', 'mx-2 mx-2badge badge-light'),
        wordKeyAndUserId = w + '#' + userId,
        countId = wordKeyAndUserId + '@' + _uuid(),
        currentCountId = 'current' + wordKeyAndUserId + '@' + _uuid();

    // Word element
    wordElement.setAttribute('type', 'button');
    // Word click count element
    wCountEle.setAttribute('id', countId);
    wCurrentCountEle.setAttribute('id', currentCountId);

    wordElement.append(wCountEle);
    wordElement.append(wCurrentCountEle);
    wordElement.addEventListener('click', ev => {
      ev.preventDefault();
      fetch('/word/update-count/save', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json; charset=utf-8'
        },
        body: JSON.stringify({
          key: wordKeyAndUserId,
          count: 1,
          currentCount: 1,
          userId: userId,
          gender: gender,
          userName: userName,
        })
      })
      .then(function (response) {
        return response.json();
      })
      .then(function (response) {
        document.querySelectorAll('span[id^="' + wordKeyAndUserId + '"]').forEach(
            function (ele) {
              ele.innerHTML = response.count;
            });
        document.querySelectorAll('span[id^="current' + wordKeyAndUserId + '"]').forEach(
            function (ele) {
              ele.innerHTML = response.currentCount;
            });
      })
      .catch(function (error) {
        console.log(error);
      });
    });
    return wordElement;
  });
}

function initWordCountRecord(userId) {
  fetch('/word/query/' + userId, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
  .then(response => response.json())
  .then(response => {
    response.forEach(r => {
      document.querySelectorAll('span[id^="' + r.key + '"]').forEach(ele => {
        return ele.innerHTML = r.count;
      });
      document.querySelectorAll('span[id^="current' + r.key + '"]').forEach(ele => {
        return ele.innerHTML = r.currentCount;
      });
    });
  })
  .catch(console.error);
}