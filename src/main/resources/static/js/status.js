function clickHandler(wordKeyAndUserId, userId, gender) {
  return () => {

    fetch('/word/update-count/clear', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify({
        key: wordKeyAndUserId,
        count: 0,
        currentCount: 0,
        userId: userId,
        gender: gender,
      })
    })
    .then(response => response.json())
    .then(response => {
      document.querySelectorAll('span[id^="' + wordKeyAndUserId + '"]').forEach(
          ele => ele.innerHTML = response.count);
      document.querySelectorAll('span[id^="current' + wordKeyAndUserId + '"]').forEach(
          ele => ele.innerHTML = response.currentCount);
    })
    .catch(console.log);
  };
}

function getWordElements(dbWords, gender) {
  return dbWords.map(w => {
    let wordElement = elementCreator('button', w.key.replace(/(.*)#[\d]+/, '$1'), 'mx-2 btn btn-primary'),
        wCountEle = elementCreator('span', w.count, 'mx-2 mx-2badge badge-light click-count'),
        wCurrentCountEle = elementCreator('span', w.currentCount, 'mx-2 mx-2badge badge-light click-count'),
        countId = w.key + '@' + _uuid(),
        currentCountId = 'current' + w.key + '@' + _uuid();

    // Word element
    wordElement.setAttribute('type', 'button');
    // Word click count element
    wCountEle.setAttribute('id', countId);
    wCurrentCountEle.setAttribute('id', currentCountId);

    wordElement.append(wCountEle);
    wordElement.append(wCurrentCountEle);
    wordElement.addEventListener('click', clickHandler(w.key, w.userId, gender));
    return wordElement;
  });
}

function fetchAllWordsByUserAccountPromise(userId) {
  return new Promise((res, rej) => {
    fetch('/word/query/' + userId, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=utf-8'
      }
    })
    .then(response => response.json())
    .then(response => res(response))
    .catch(console.error);

  });
}