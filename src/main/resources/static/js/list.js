function getWordElements(dbWords) {
  return dbWords.map(w => {
    let wordElement = document.createElement('button'),
        wordClickCountElement = document.createElement('span'),
        wordKeyAndUserId = w.key,
        wordKeyAndUserIdAndUuid = wordKeyAndUserId + '@' + _uuid();
    // Set word's key and user' id
    // ex: Life#27 means word is "Life" and userId is 27

    // Word element
    wordElement.innerHTML = wordKeyAndUserId.replace(/(.*)#[\d]+/, '$1');
    wordElement.setAttribute('class', 'mx-2 btn btn-primary');
    wordElement.setAttribute('type', 'button');
    // Word click count element
    wordClickCountElement.setAttribute('id', wordKeyAndUserIdAndUuid);
    wordClickCountElement.setAttribute('class', 'mx-2 mx-2badge badge-light click-count');
    wordClickCountElement.innerHTML = w.count;
    wordElement.append(wordClickCountElement);
    return wordElement;
  });
}

function fetchAllWordsByUserAccountPromise(userId) {
  return new Promise((res, rej) => {
    fetch('/word/queryAll/' + userId, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=utf-8'
      }
    })
    .then(response => response.json())
    .then(response => res(response))
    .catch(console.error);

  });
}

function getCompareTo(currentUserAggCount = 0, userGender = 'MALE') {
  return (left, right) => {
    let leftDistance = Math.abs(left.aggCount - currentUserAggCount),
        rightDistance = Math.abs(right.aggCount - currentUserAggCount),
        leftGenderCode = (left.gender == userGender ? 0 : 1),
        rightGenderCode = (right.gender == userGender ? 0 : 1)
    ;

    return leftDistance === rightDistance ?
        leftGenderCode - rightGenderCode :
        leftDistance - rightDistance;
  };
}

function getAccumulator() {
  return (m, o) => {
    let originMap = m.get(o.userId);
    if (!originMap) {
      m.set(o.userId, {
        aggCount: o.count,
        data: [{...o}],
        userId: o.userId,
        userName: o.userName,
        gender: o.gender,
      });
    } else {
      m.set(o.userId, {
        aggCount: originMap.aggCount + o.count,
        data: [...originMap.data, o],
        userId: o.userId || originMap.userId,
        userName: o.userName || originMap.userName,
        gender: o.gender || originMap.gender,
      });
    }
    return m;
  };
}

function getWordDomDecorator() {
  return acc => {
    let wrapper = elementCreator('tr');
    wrapper.setAttribute('id', acc.userId);

    let duplicateObj = document.querySelectorAll('tr[id="'+ acc.userId +'"]');
    duplicateObj.forEach(a => a.remove());

    let userNameTd = elementCreator('td');
    userNameTd.append(elementCreator('span', acc.userName, 'user-name'));

    let genderTd = elementCreator('td');
    genderTd.append(elementCreator('span', acc.gender, 'gender'));

    let aggCountTd = elementCreator('td');
    aggCountTd.append(elementCreator('span', acc.aggCount, 'agg-count'));

    let wordsTd = elementCreator('td');
    getWordElements(acc.data).forEach(e => wordsTd.append(e));

    wrapper.append(userNameTd);
    wrapper.append(genderTd);
    wrapper.append(aggCountTd);
    wrapper.append(wordsTd);
    return wrapper;
  };
}