package com.example.clickboard.generator;

import com.example.clickboard.service.SequenceIdService;
import org.springframework.data.mongodb.core.mapping.Document;

public interface CrudIdGeneratable<T> {

  default Long getNextSequenceId(SequenceIdService sequenceIdService, T form) {
    return sequenceIdService.getNextSequenceId(form.getClass().getAnnotation(Document.class).collection());
  }


}
