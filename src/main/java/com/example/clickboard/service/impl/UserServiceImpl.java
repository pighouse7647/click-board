package com.example.clickboard.service.impl;

import com.example.clickboard.entity.User;
import com.example.clickboard.generator.CrudIdGeneratable;
import com.example.clickboard.repository.UserRepository;
import com.example.clickboard.service.SequenceIdService;
import com.example.clickboard.service.UserService;
import java.util.UUID;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class UserServiceImpl implements UserService, CrudIdGeneratable {

  private final UserRepository repository;
  private final SequenceIdService sequenceIdService;
  private final BCryptPasswordEncoder passwordEncoder;


  public UserServiceImpl(UserRepository repository, SequenceIdService sequenceIdService, BCryptPasswordEncoder passwordEncoder) {
    this.repository = repository;
    this.sequenceIdService = sequenceIdService;
    this.passwordEncoder = passwordEncoder;
  }

  public Mono<User> insert(User user) {
    user.setId(this.getNextSequenceId(sequenceIdService, user));
    user.setKey(UUID.randomUUID().toString());
    user.setPassword(passwordEncoder.encode(user.getPassword()));
    user.setRole("ROLE_MEMBER");
    return Mono.just(repository.insert(user));
  }
  public Mono<User> update(User word) {
    return Mono.just(repository.save(word));
  }

  @Override
  public Mono<User> getByKey(String key) {
    return Mono.justOrEmpty(repository.findFirstByKey(key));
  }

  @Override
  public Mono<User> getByAccount(String account) {
    return Mono.justOrEmpty(repository.findByAccount(account));
  }

  @Override
  public Mono<User> getById(Long currentUserId) {
    return Mono.justOrEmpty(repository.findById(currentUserId));
  }

}
