package com.example.clickboard.service.impl;

import com.example.clickboard.entity.Word;
import com.example.clickboard.generator.CrudIdGeneratable;
import com.example.clickboard.repository.WordRepository;
import com.example.clickboard.service.SequenceIdService;
import com.example.clickboard.service.WordService;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class WordServiceImpl implements WordService, CrudIdGeneratable {

  private final WordRepository repository;
  private final SequenceIdService sequenceIdService;


  public WordServiceImpl(WordRepository repository, SequenceIdService sequenceIdService) {
    this.repository = repository;
    this.sequenceIdService = sequenceIdService;
  }

  public Mono<Word> insert(Word word) {
    word.setId(this.getNextSequenceId(sequenceIdService, word));
    return Mono.just(repository.save(word));
  }

  public Mono<Word> update(Word word) {
    return Mono.just(repository.save(word));
  }

  @Override
  public Mono<Word> getByKey(String key) {
    return Mono.justOrEmpty(repository.findFirstByKey(key));
  }

  @Override
  public Flux<Word> getByUserId(Long userId) {
    return Flux.fromIterable(repository.findAllByUserId(userId));
  }

  @Override
  public Flux<Word> getAll() {
    return Flux.fromIterable(repository.findAll());
  }

}
