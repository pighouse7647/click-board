package com.example.clickboard.service;

import com.example.clickboard.entity.Word;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface WordService {

  Mono<Word> insert(Word word);

  Mono<Word> update(Word word);

  Mono<Word> getByKey(String key);

  Flux<Word> getByUserId(Long userId);

  Flux<Word> getAll();
}
