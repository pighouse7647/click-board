package com.example.clickboard.service;

import com.example.clickboard.entity.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import reactor.core.publisher.Mono;

public interface UserService {

  Mono<User> insert(User word);

  Mono<User> update(User word);

  Mono<User> getByKey(String key);

  Mono<User> getByAccount(String account);

  Mono<User> getById(Long currentUserId);
}
