package com.example.clickboard;

import com.example.clickboard.entity.SequenceId;
import com.example.clickboard.entity.User;
import com.example.clickboard.entity.Word;
import com.example.clickboard.repository.SequenceIdRepository;
import java.util.Optional;
import org.owasp.html.HtmlPolicyBuilder;
import org.owasp.html.PolicyFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.mapping.Document;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

@SpringBootApplication
public class ClickBoardApplication {

  public static void main(String[] args) {
    SpringApplication.run(ClickBoardApplication.class, args);
  }



  @Bean
  CommandLineRunner init(SequenceIdRepository sequenceIdRepository) {
    return args -> {
      initSequence(sequenceIdRepository, Word.class.getAnnotation(Document.class).collection());
      initSequence(sequenceIdRepository, User.class.getAnnotation(Document.class).collection());
    };
  }

  private void initSequence(SequenceIdRepository sequenceIdRepository, String collectionName) {
    final Long maxSeq = Optional.ofNullable(sequenceIdRepository.findFirstByIdOrderBySeqDesc(collectionName)).map(SequenceId::getSeq).orElse(0L);
    SequenceId sequenceId = new SequenceId();
    sequenceId.setId(collectionName);
    sequenceId.setSeq(maxSeq);
    sequenceIdRepository.save(sequenceId);
  }


  @Bean
  public Scheduler scheduler() {
    return Schedulers.elastic();
  }


  @Bean
  public PolicyFactory htmlPolicy() {
    return new HtmlPolicyBuilder()
        .allowElements("a", "b", "i", "del", "pre", "code")
        .allowUrlProtocols("http", "https")
        .allowAttributes("href").onElements("a")
        .requireRelNofollowOnLinks()
        .toFactory();
  }
}

