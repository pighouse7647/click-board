package com.example.clickboard.repository;

import com.example.clickboard.entity.Word;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;

public interface WordRepository extends MongoRepository<Word, Long> {

  Word findFirstByKey(@Param("key") String key);
  List<Word> findAllByUserId(@Param("userId") Long userId); // FIXME Still one result
}
