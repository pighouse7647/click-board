package com.example.clickboard.repository;

import com.example.clickboard.entity.SequenceId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SequenceIdRepository extends MongoRepository<SequenceId, String> {

  SequenceId findFirstById(String id);
  SequenceId findFirstByIdOrderBySeqDesc(String id);
}
