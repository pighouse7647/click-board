package com.example.clickboard.repository;

import com.example.clickboard.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends MongoRepository<User, Long> {

  User findFirstByKey(@Param("key") String key);

  User findByAccount(@Param("account") String account);
}
