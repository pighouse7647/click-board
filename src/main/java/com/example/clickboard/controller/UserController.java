package com.example.clickboard.controller;

import com.example.clickboard.entity.User;
import com.example.clickboard.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/user")
public class UserController {

  @Value("${server.port}")
  private String serverPort;
  private final UserService service;
  private final ObjectMapper mapper;

  public UserController(UserService service, ObjectMapper mapper) {
    this.service = service;
    this.mapper = mapper;
  }

  @PostMapping("/save")
  public Mono<User> save(@RequestBody final User user) {
    Mono<User> wordMono = service.getByKey(user.getKey()).defaultIfEmpty(new User());
    return wordMono.flatMap(dbUser -> {
      if (dbUser.getId() == null) {
        return Mono.error(new Error("Go to registry page."));
      } else {
        return this.service.update(dbUser);
      }
    });
  }

  @PostMapping("/insert")
  public Mono<User> insert(@RequestBody final User user) {
    WebClient client = WebClient.create();

    // HTTP POST
    Mono<User> createdUser = client
        .post()
        .uri("http://localhost:" + serverPort + "/user/save")
        .accept(MediaType.APPLICATION_JSON)
        .body(Mono.just(user), User.class)
        .exchange()
        .flatMap(res -> res.bodyToMono(User.class))
        .doOnSuccess(w -> {

        });

    return createdUser;
  }

  private String toJSON(@RequestBody User user) {
    try {
      return mapper.writeValueAsString(user);
    } catch (JsonProcessingException e) {
      throw new RuntimeException(e);
    }
  }

}
