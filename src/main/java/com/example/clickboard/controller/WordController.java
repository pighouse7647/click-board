package com.example.clickboard.controller;

import com.example.clickboard.entity.MultiWord;
import com.example.clickboard.entity.User;
import com.example.clickboard.entity.Word;
import com.example.clickboard.service.UserService;
import com.example.clickboard.service.WordService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.URI;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.client.ReactorNettyWebSocketClient;
import org.springframework.web.reactive.socket.client.WebSocketClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/word")
public class WordController {

  @Value("${server.port}")
  private String serverPort;
  private final WordService service;
  private final ObjectMapper mapper;
  private final UserService userService;
  private final ReactiveUserDetailsService userDetailsService;

  public WordController(WordService service, ObjectMapper mapper, UserService userService, ReactiveUserDetailsService userDetailsService) {
    this.service = service;
    this.mapper = mapper;
    this.userService = userService;
    this.userDetailsService = userDetailsService;
  }

  @GetMapping("/query/{userId}")
  public Flux<Word> queryByUserId(@PathVariable Long userId) {
    return service.getByUserId(userId);
  }

  @GetMapping("/queryAll/{currentUserId}")
  public Flux<Word> queryAll(@PathVariable Long currentUserId) {
    Mono<User> user = userService.getById(currentUserId)
        .flatMap(u -> Mono.just(u));
    return service.getAll();
  }

  @GetMapping("/queryAll")
  public Flux<Word> queryAll() {
    return service.getAll();
  }

  @PostMapping("/save")
  public Mono<Word> save(@RequestBody final Word word) {
    Mono<Word> wordMono = service.getByKey(word.getKey()).defaultIfEmpty(new Word());
    return wordMono.flatMap(dbWord -> {
      dbWord.setCount(dbWord.getCount() + 1);
      dbWord.setCurrentCount(dbWord.getCurrentCount() + 1);
      if (dbWord.getId() == null) {
        return this.service.insert(word);
      } else {
        return this.service.update(dbWord);
      }
    });
  }

  @PostMapping("/clear")
  public Mono<Word> clear(@RequestBody final Word word) {
    Mono<Word> wordMono = service.getByKey(word.getKey()).defaultIfEmpty(new Word());
    return wordMono.flatMap(dbWord -> {
      dbWord.setCurrentCount(0);
      if (dbWord.getId() == null) {
        return this.service.insert(word);
      } else {
        return this.service.update(dbWord);
      }
    });
  }

  @PostMapping("/update-count/{manipulate}")
  public Mono<Word> updateCount(@RequestBody final Word word, @PathVariable final String manipulate) {

    // HTTP POST
    return WebClient.create()
        .post()
        .uri("http://localhost:" + serverPort + "/word/" + manipulate)
        .accept(MediaType.APPLICATION_JSON)
        .body(Mono.just(word), Word.class)
        .exchange()
        .flatMap(res -> res.bodyToMono(Word.class))
        .doOnSuccess(w -> {
          // WS PUBLISH
          WebSocketClient wsClient = new ReactorNettyWebSocketClient();
          wsClient.execute(
              URI.create("ws://localhost:"+serverPort+"/ws/word"),
              session -> session.send(
                  Mono.just(session.textMessage(this.toJSON(w))))
                  .thenMany(session.receive()
                      .map(WebSocketMessage::getPayloadAsText)
                      .log())
                  .then())
              .subscribe();
        })
        .doOnSuccess(w -> {
          this.queryAll().reduceWith(MultiWord::new, (initObj, iterObj) -> {
            initObj.getWords().add(iterObj);
            return initObj;
          })
              .map(this::toJSON)
              .subscribe(data -> {
                WebSocketClient wsClient = new ReactorNettyWebSocketClient();
                wsClient.execute(
                    URI.create("ws://localhost:" + serverPort + "/ws/words"),
                    session -> session.send(
                        Mono.just(session.textMessage(data)))
                        .thenMany(session.receive()
                            .map(WebSocketMessage::getPayloadAsText)
                            .log())
                        .then())
                    .subscribe();
              });
        });
  }


  private String toJSON(@RequestBody Word word) {
    try {
      return mapper.writeValueAsString(word);
    } catch (JsonProcessingException e) {
      throw new RuntimeException(e);
    }
  }

  private String toJSON(@RequestBody MultiWord words) {
    try {
      return mapper.writeValueAsString(words);
    } catch (JsonProcessingException e) {
      throw new RuntimeException(e);
    }
  }

}
