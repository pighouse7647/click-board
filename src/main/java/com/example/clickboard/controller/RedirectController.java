package com.example.clickboard.controller;

import com.example.clickboard.entity.User;
import com.example.clickboard.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.reactive.result.view.RedirectView;
import reactor.core.publisher.Mono;

@Controller
@RequestMapping("/redirect")
public class RedirectController {

  private final UserService service;

  public RedirectController(UserService service) {
    this.service = service;
  }

  @PostMapping("/registry")
  public RedirectView registry(final User user, Model model) {
    Mono<User> userMono = service.getByKey(user.getKey()).defaultIfEmpty(new User()).flatMap(dbUser -> {
      if (dbUser.getId() == null) {
        return this.service.insert(user);
      } else {
        return Mono.error(new Error("Already exist"));
      }
    });
    userMono.subscribe(a -> {
    });
    return new RedirectView("/");
  }
}
