package com.example.clickboard.handler;

import com.example.clickboard.entity.Word;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Optional;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.UnicastProcessor;

public class WordSocketHandler implements WebSocketHandler {

  private final ObjectMapper mapper;
  private UnicastProcessor<Word> messagePublisher;
  private Flux<Word> messages;

  public WordSocketHandler(UnicastProcessor<Word> messagePublisher, Flux<Word> messages) {
    this.mapper = new ObjectMapper();
    this.messagePublisher = messagePublisher;
    this.messages = messages;
  }

  @Override
  public Mono<Void> handle(WebSocketSession session) {
    WebSocketWordSubscriber subscriber = new WebSocketWordSubscriber(messagePublisher);
    session.receive()
        .map(WebSocketMessage::getPayloadAsText)
        .map(this::toWord)
        .subscribe(subscriber::onNext, subscriber::onError, subscriber::onComplete);
    return session.send(messages.map(this::toJSON).map(session::textMessage));
  }

  private static class WebSocketWordSubscriber {

    private UnicastProcessor<Word> messagePublisher;
    private Optional<Word> lastReceivedMessage = Optional.empty();

    public WebSocketWordSubscriber(UnicastProcessor<Word> messagePublisher) {
      this.messagePublisher = messagePublisher;
    }

    public void onNext(Word message) {
      lastReceivedMessage = Optional.of(message);
      messagePublisher.onNext(message);
    }

    public void onError(Throwable error) {
      error.printStackTrace();
    }

    public void onComplete() {
      lastReceivedMessage.ifPresent(messagePublisher::onNext);
    }
  }


  private Word toWord(String json) {
    try {
      return mapper.readValue(json, Word.class);
    } catch (IOException e) {
      throw new RuntimeException("Invalid JSON:" + json, e);
    }
  }

  private String toJSON(Word message) {
    try {
      return mapper.writeValueAsString(message);
    } catch (JsonProcessingException e) {
      throw new RuntimeException(e);
    }
  }
}
