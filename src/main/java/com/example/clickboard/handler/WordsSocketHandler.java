package com.example.clickboard.handler;

import com.example.clickboard.entity.MultiWord;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Optional;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.UnicastProcessor;

public class WordsSocketHandler implements WebSocketHandler {

  private final ObjectMapper mapper;
  private UnicastProcessor<MultiWord> messagePublisher;
  private Flux<MultiWord> messages;

  public WordsSocketHandler(UnicastProcessor<MultiWord> messagePublisher, Flux<MultiWord> messages) {
    this.mapper = new ObjectMapper();
    this.messagePublisher = messagePublisher;
    this.messages = messages;
  }

  @Override
  public Mono<Void> handle(WebSocketSession session) {
    WebSocketWordsSubscriber subscriber = new WebSocketWordsSubscriber(messagePublisher);
    session.receive()
        .map(WebSocketMessage::getPayloadAsText)
        .map(this::toWord)
        .subscribe(subscriber::onNext, subscriber::onError, subscriber::onComplete);
    return session.send(messages.map(this::toJSON).map(session::textMessage));
  }

  private static class WebSocketWordsSubscriber {

    private UnicastProcessor<MultiWord> messagePublisher;
    private Optional<MultiWord> lastReceivedMessage = Optional.empty();

    public WebSocketWordsSubscriber(UnicastProcessor<MultiWord> messagePublisher) {
      this.messagePublisher = messagePublisher;
    }

    public void onNext(MultiWord message) {
      lastReceivedMessage = Optional.of(message);
      messagePublisher.onNext(message);
    }

    public void onError(Throwable error) {
      error.printStackTrace();
    }

    public void onComplete() {
      lastReceivedMessage.ifPresent(messagePublisher::onNext);
    }
  }


  private MultiWord toWord(String json) {
    try {
      return mapper.readValue(json, MultiWord.class);
    } catch (IOException e) {
      throw new RuntimeException("Invalid JSON:" + json, e);
    }
  }

  private String toJSON(MultiWord message) {
    try {
      return mapper.writeValueAsString(message);
    } catch (JsonProcessingException e) {
      throw new RuntimeException(e);
    }
  }
}
