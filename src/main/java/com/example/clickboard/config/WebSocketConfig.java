package com.example.clickboard.config;

import com.example.clickboard.entity.MultiWord;
import com.example.clickboard.entity.Word;
import com.example.clickboard.handler.WordSocketHandler;
import com.example.clickboard.handler.WordsSocketHandler;
import java.util.HashMap;
import java.util.Map;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.HandlerMapping;
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping;
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter;
import reactor.core.publisher.Flux;
import reactor.core.publisher.UnicastProcessor;

@Configuration
public class WebSocketConfig {
  @Bean
  public UnicastProcessor<Word> messagePublisher() {
    return UnicastProcessor.create();
  }

  @Bean
  public UnicastProcessor<MultiWord> messagesPublisher() {
    return UnicastProcessor.create();
  }

  @Bean
  public Flux<Word> messages(UnicastProcessor<Word> messagePublisher) {
    return messagePublisher
        .replay(25)
        .autoConnect();
  }

  @Bean
  public Flux<MultiWord> messagess(UnicastProcessor<MultiWord> messagesPublisher) {
    return messagesPublisher
        .replay(25)
        .autoConnect();
  }

  @Bean
  public HandlerMapping webSocketMapping(UnicastProcessor<Word> messagePublisher, UnicastProcessor<MultiWord> messagesPublisher, Flux<Word> messages, Flux<MultiWord> messagess) {
    Map<String, Object> map = new HashMap<>();
    map.put("/ws/word", new WordSocketHandler(messagePublisher, messages));
    map.put("/ws/words", new WordsSocketHandler(messagesPublisher, messagess));
    SimpleUrlHandlerMapping simpleUrlHandlerMapping = new SimpleUrlHandlerMapping();
    simpleUrlHandlerMapping.setUrlMap(map);
    simpleUrlHandlerMapping.setOrder(10);
    return simpleUrlHandlerMapping;
  }

  @Bean
  public WebSocketHandlerAdapter handlerAdapter() {
    return new WebSocketHandlerAdapter();
  }

}
