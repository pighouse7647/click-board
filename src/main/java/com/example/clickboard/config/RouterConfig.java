package com.example.clickboard.config;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;

import com.example.clickboard.entity.User;
import com.example.clickboard.repository.UserRepository;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

@Configuration
@Slf4j
public class RouterConfig {

  @Bean
  public RouterFunction<ServerResponse> routes(UserRepository userRepository) {
    return RouterFunctions
        .route(
            GET("/"),
            request -> ServerResponse.ok().body(BodyInserters.fromResource(new ClassPathResource("templates/index.html")))
        )
        .andRoute(
            GET("/registry"),
            request -> ServerResponse.ok().body(BodyInserters.fromResource(new ClassPathResource("templates/registry.html")))
        )
        .andRoute(
            GET("/read"),
            request -> getServerResponseMono(userRepository, "read")
        )
        .andRoute(
            GET("/status"),
            request -> getServerResponseMono(userRepository, "status")
        )
        .andRoute(
            GET("/list"),
            request -> getServerResponseMono(userRepository, "list")
        );
  }

  private Mono<ServerResponse> getServerResponseMono(UserRepository userRepository, String targetTemplateName) {
    return ReactiveSecurityContextHolder.getContext()
        .switchIfEmpty(Mono.error(new IllegalStateException("ReactiveSecurityContext is empty")))
        .map(SecurityContext::getAuthentication)
        .map(Authentication::getPrincipal)
        .doOnError(Throwable::printStackTrace)
        .doOnSuccess(System.out::println)
        .flatMap(userdetail -> {
          org.springframework.security.core.userdetails.User userDetail = (org.springframework.security.core.userdetails.User) userdetail;
          User user = userRepository.findByAccount(userDetail.getUsername());
          HashMap<String, Object> model = new HashMap<>();
          model.put("userDetail", userDetail);
          model.put("user", user);
          model.put("currentTemplate", targetTemplateName);
          return ServerResponse.ok().render(targetTemplateName, model);
        });
  }
}
