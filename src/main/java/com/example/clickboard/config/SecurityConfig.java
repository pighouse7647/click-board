package com.example.clickboard.config;

import com.example.clickboard.repository.UserRepository;
import java.net.URI;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UserDetailsRepositoryReactiveAuthenticationManager;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.server.DefaultServerRedirectStrategy;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.ServerRedirectStrategy;
import org.springframework.security.web.server.authentication.RedirectServerAuthenticationSuccessHandler;
import org.springframework.security.web.server.authentication.logout.RedirectServerLogoutSuccessHandler;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

@EnableWebFluxSecurity
public class SecurityConfig {

  @Bean
  public ReactiveUserDetailsService userDetailsService(UserRepository userRepository, Scheduler scheduler) {
    return accountName -> {
      return Mono.defer(() -> {
        return Mono.justOrEmpty(userRepository.findByAccount(accountName))
            .map(acct -> {
              UserDetails member = org.springframework.security.core.userdetails.User.
                  withUsername(accountName)
                  .password(passwordEncoder().encode(acct.getPassword()))
                  .password(acct.getPassword())
                  .roles("MEMBER")
                  .build();
              return member;
            });

      }).subscribeOn(scheduler);
    };
  }

  @Bean
  public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity http, ReactiveAuthenticationManager authenticationManager) {
    ServerRedirectStrategy redirectStrategy = new DefaultServerRedirectStrategy();

    RedirectServerLogoutSuccessHandler logoutSuccessHandler = new RedirectServerLogoutSuccessHandler();
    logoutSuccessHandler.setLogoutSuccessUrl(URI.create("/?logout"));

    return http
        .headers()
//            .contentTypeOptions().disable()
            .xssProtection().disable()
//            .frameOptions()
//            .mode(XFrameOptionsServerHttpHeadersWriter.Mode.SAMEORIGIN)
            .cache().disable()
//        .contentSecurityPolicy("script-src 'unsafe-inline' http://localhost:8080/reader/js/index.js; style-src 'unsafe-inline' http://localhost:8080/reader/css/index.css")
//        .and()
        .and()
        .authenticationManager(authenticationManager)
            .authorizeExchange()
//            .pathMatchers("/**")
//            .authenticated()
//            .pathMatchers("/index", "/registry")
//            .permitAll()
            .pathMatchers("/", "/read", "/list", "/list/**", "/status/**", "/status", "/logout").hasRole("MEMBER")
            .anyExchange().permitAll()
        .and()
        .formLogin()
//        .loginPage("/")
            .authenticationSuccessHandler(new RedirectServerAuthenticationSuccessHandler())
            .authenticationFailureHandler((webFilterExchange, ex) -> {
              ServerWebExchange webExchange = webFilterExchange.getExchange();
              return webExchange
                  .getFormData()
                  .map(formData -> URI.create(String.format("/?account=%s&error", formData.getFirst("account"))))
                  .flatMap(uri -> redirectStrategy.sendRedirect(webExchange, uri));
            })
        .and()
        .logout()
            .logoutUrl("/logout")
            .logoutSuccessHandler(logoutSuccessHandler)
        .and()
        .csrf().disable()
        .build();
  }

  @Bean
  public ReactiveAuthenticationManager authenticationManager(ReactiveUserDetailsService userDetailsService, PasswordEncoder passwordEncoder) {
    UserDetailsRepositoryReactiveAuthenticationManager manager = new UserDetailsRepositoryReactiveAuthenticationManager(userDetailsService);
    manager.setPasswordEncoder(passwordEncoder);
    return manager;
  }

  @Bean
  public BCryptPasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();

  }

}
