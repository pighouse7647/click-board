package com.example.clickboard.entity;

import com.example.clickboard.enums.Gender;
import javax.validation.constraints.NotEmpty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.web.bind.annotation.SessionAttributes;


@Data
@NoArgsConstructor
@Document(collection = "user")
@SessionAttributes({"path", "user"})
public class User {

  @Id
  private Long id;

  @Indexed(unique = true)
  private String key;

  @NotEmpty
  private String account;

  private String password;

  private Gender gender;

  private String role;

  public User(String account, String password, Gender gender, String role) {
    this.account = account;
    this.password = password;
    this.gender = gender;
    this.role = role;
  }

  public User(String account, String password, Gender gender) {
    this(account, password, gender, "ROLE_MEMBER");
  }

}
