package com.example.clickboard.entity;

import com.example.clickboard.enums.Gender;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;


@Data
@Document(collection = "word")
public class Word {
  @Id
  private Long id;

  @Indexed(unique = true)
  private String key; // Compound with user's id and word value

  private int count;

  private int currentCount;

  private Long userId;

  private Gender gender;

  private String userName;

}
